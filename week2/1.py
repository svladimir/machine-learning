import re as re
import numpy as np
import scipy as sc
from scipy import spatial
file_obj = open('sentences.txt')
dict_word={}
clear_str=[]
data_list = file_obj.readlines()
for line in data_list:
    str_first=re.split('[^a-z]',line.strip().lower())
    str_cl=[x for x in str_first if len(x)>0]
    clear_str.append(str_cl)
    for str_word in str_cl:
        if dict_word.get(str_word,0)==0:
            dict_word[str_word]=len(dict_word)
    #print dict_word
#print(clear_str)
cnt_line=0
cnt_column=0
a=np.zeros((len(clear_str), len(dict_word)))
for line in clear_str:
    for word in dict_word:
        #print line.count(word)
        a[cnt_line][cnt_column]=line.count(word)
       # print(a[cnt_line][cnt_column])
        cnt_column=cnt_column+1
        #print(cnt_column)
    cnt_column=0
    cnt_line=cnt_line+1
a0=a[0]
min_ugol=[]

for str_arr in a:
    min_ugol.append(sc.spatial.distance.cosine(a0,str_arr))
str_arr=min_ugol.copy()
min_ugol.sort()
file_obj = open('answer.txt', 'w')
str1=''

cnt=0
for value in min_ugol:
    if value!=0.0 and cnt<3:
        idx=(str_arr.index(value))
        str1=str1+' '+str(idx)
    cnt=cnt+1
file_obj.write(str1.strip())
file_obj.close()