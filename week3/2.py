#!/usr/bin/env python
# coding: utf-8

# In[38]:


from matplotlib import pylab as plt
import math
import numpy as np
import scipy
from scipy import linalg
from scipy import optimize



# In[73]:


x=np.arange(1,15,0.1)
y=math.sin(x)


# In[76]:


x=np.arange(1,15,0.1)
y= np.sin(x / 5) * np.exp(x / 10) + 5 * np.exp(-x / 2)
plt.plot(x, y)
plt.show()


# In[85]:


x=np.arange(1,16,1)
y= np.sin(x / 5) * np.exp(x / 10) + 5 * np.exp(-x / 2)
plt.plot(x, y)
plt.show()


# # x=1

# In[87]:


x=1.0
y= np.sin(x / 5) * np.exp(x / 10) + 5 * np.exp(-x / 2)
y


# # x=15

# In[88]:


x= 15.0
y= math.sin(x / 5) * math.exp(x / 10) + 5 * math.exp(-x / 2)
print(y)


# In[89]:


x= 8.0
y= math.sin(x / 5) * math.exp(x / 10) + 5 * math.exp(-x / 2)
print(y)


# In[90]:


x= 4.0
y= math.sin(x / 5) * math.exp(x / 10) + 5 * math.exp(-x / 2)
print(y)


# In[97]:


x= 10.0
y= math.sin(x / 5) * math.exp(x / 10) + 5 * math.exp(-x / 2)
print(y)


# # w0+w1x1=f(x)

# In[39]:


a = np.array([[1, 1], [1, 15]])
b = np.array([3.252216865271419, 0.635221419579])

x = linalg.solve(a, b)
print x


# In[45]:


x=np.arange(1,15,1)
fx=1.94326263-0.10386542*x
plt.plot(x, fx)
plt.show()


# # w0+w1x1+w2x1**2=f(x)
# 

# In[94]:


a = np.array([[1, 1, 1],[1,8,64], [1, 15,225]])
b = np.array([3.252216865271419,2.31617015905, 0.635221419579])

x = linalg.solve(a, b)
print x


# In[99]:


x=np.arange(1,16,1)
fx=4.36264154-1.29552587*x+0.19333685*x**2-0.00823565*x**3
plt.plot(x, fx)
plt.show()


# In[32]:


print(y)


# In[98]:


a = np.array([[1, 1, 1,1],[1,4,16,64],[1,10,100,1000], [1, 15,225,3375]])
b = np.array([3.252216865271419,1.74684594959,2.505416407, 0.635221419579])

x = linalg.solve(a, b)
print x


# In[34]:


x= 15
y= math.sin(x / 5) * math.exp(x / 10) + 5 * math.exp(-x / 2)


# In[35]:


print(y)

